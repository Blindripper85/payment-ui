/* eslint-disable */

const webpack = require('webpack');
const { resolve } = require('path');
const common = require('./common.js');
const dotenv = require('dotenv');

let envs = {};
const result = dotenv.config();
if (result.parsed) {
  envs = result.parsed;
}

module.exports = Object.assign({}, common, {
  mode: 'development',
  watch: true,
  watchOptions: {
    aggregateTimeout: 3000,
    poll: true,
    ignored: /node_modules/
  },
  plugins: [
    new webpack.EnvironmentPlugin({
      NODE_ENV: 'development',
      WORDPRESS: true,
      ...envs,
    }),
  ],
  devtool: 'source-map',
  devServer: {
    contentBase: resolve(__dirname, '..', 'build'),
    compress: false,
    port: process.env.NODE_PORT,
    historyApiFallback: true,
  },
})
