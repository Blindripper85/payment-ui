import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import cancel from 'Root/assets/images/cancel.png';
import styles from './styles.less';

const Expired = (props) => (
  <>

    <div className="row justify-content-center align-items-center h-100">
      <div className="card my-4" style={{ width: "500px" }}>
        <div className={styles['card-body']}>
          <img
            src={cancel}
            alt="medal"
            className="d-block mx-auto"
            width="59px"
            height="59px"
          />
          <h1 className={styles.title}>Expired transaction</h1>
          <p className={styles.explain}>Your transaction timed out.</p>
        </div>
      </div>
    </div>
  </>
);

Expired.propTypes = {};

export default Expired;
