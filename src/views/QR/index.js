import React from 'react';
import logo from 'Root/assets/images/logo.png';
import QRCode from 'qrcode.react';
import CountDownDuration from 'Root/shared/components/CountDownDuration';
import CopyInfo from 'Root/shared/components/CopyInfo';
import styles from './styles.less';
import { paymentDurationAsSeconds } from 'Root/constants/values';


export default class QR extends React.Component {

  constructor(props) {
    super(props);
    this.props = props;
    console.log(props);
    this.conseil = {
      server: {
        url: PAYZOS_TEZOS_ENDPOINT,
        apiKey: PAYZOS_TEZOS_APIKEY,
      },
      destination_hash: this.props.destination_hash,
      amount: this.props.amount,
      show_amount: this.props.amount / 1000000,
      payment_id: this.props.payment_id,
      start_time: this.props.start_time,
    };

    console.log(Math.floor(((this.conseil.start_time + (paymentDurationAsSeconds * 1000)) - Date.now()) / 1000));
    this.state = {
      passedTime: 100,
      durationAsSecond: Math.floor(((this.conseil.start_time + (paymentDurationAsSeconds * 1000)) - Date.now()) / 1000)
    }

    this.check_transaction(this.conseil.destination_hash, this.conseil.amount, this.conseil.start_time);
  }

  /**
   * 
   * @param {*} destination_hash 
   * @param {*} amount 
   * @param {*} start_time 
   */
  async check_transaction(destination_hash, amount, start_time) {

    let transactionQuery = conseiljs.ConseilQueryBuilder.blankQuery();
    transactionQuery = conseiljs.ConseilQueryBuilder.addFields(transactionQuery, 'block_level', 'timestamp', 'source', 'destination', 'amount', 'fee', 'counter');
    transactionQuery = conseiljs.ConseilQueryBuilder.addPredicate(transactionQuery, 'kind', conseiljs.ConseilOperator.EQ, ['transaction'], false);
    transactionQuery = conseiljs.ConseilQueryBuilder.addPredicate(transactionQuery, 'timestamp', conseiljs.ConseilOperator.BETWEEN, [start_time, Date.now()], false);
    transactionQuery = conseiljs.ConseilQueryBuilder.addPredicate(transactionQuery, 'status', conseiljs.ConseilOperator.EQ, ['applied'], false);
    transactionQuery = conseiljs.ConseilQueryBuilder.addPredicate(transactionQuery, 'amount', conseiljs.ConseilOperator.EQ, [amount], false);
    transactionQuery = conseiljs.ConseilQueryBuilder.addPredicate(transactionQuery, 'destination', conseiljs.ConseilOperator.EQ, [destination_hash], false);
    transactionQuery = conseiljs.ConseilQueryBuilder.addOrdering(transactionQuery, 'block_level', conseiljs.ConseilSortDirection.DESC);
    transactionQuery = conseiljs.ConseilQueryBuilder.setLimit(transactionQuery, 5);

    const result = await conseiljs.ConseilDataClient.executeEntityQuery(this.conseil.server, "tezos", "mainnet", "operations", transactionQuery);


    if (typeof result.length !== undefined && result.length > 0) {
      return this.validate_payment();
    }
    else if ((Date.now() - this.conseil.start_time + 30000) < (paymentDurationAsSeconds * 1000)) {
      setTimeout(() => {
        return this.check_transaction(destination_hash, amount, start_time);
      }, 30000)
    }
    else if ((Date.now() - this.conseil.start_time) < (paymentDurationAsSeconds * 1000)) {
      setTimeout(() => {
        return this.check_transaction(destination_hash, amount, start_time);
      }, 5000)
    }
    else {
      return this.validate_payment();
    }
  }


  async validate_payment() {
    const payment_id = this.props.payment_id;
    const url = PAYZOS_API_URL + "validate_payment";

    const response = await fetch(url, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ payment_id: payment_id })
    });
    response;
    setTimeout(() => {
      this.payment_done();
    }, 3500)
  }

  payment_done() {
    location.reload();
  }

  render() {
    return (
      <div className="row justify-content-center align-items-center h-100">
        <div className="card my-4" style={{ width: "500px" }}>
          <div className={styles['card-header']}>
            <div className="row justify-content-between">
              <div className="col-6">
                <img src={logo} width="30px" height="40px" alt="payzos" />
              </div>
              <div className="col-6">
                <div style={{ float: "right" }}>
                  <CountDownDuration
                    duration={this.state.durationAsSecond}
                    passedTime={this.state.passedTime}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className={styles['card-body']}>
            <h2 className={styles.title}>
              Please send <span>{this.conseil.show_amount} XTZ</span> to belew address
              </h2>
            <div className={styles['small-card']}>
              <div className="row">
                <div className="col-12">
                  <QRCode
                    value={this.conseil.destination_hash}
                    size={210}
                    fgColor="#377cf2"
                    className="d-block mx-auto"
                  />
                </div>
              </div>
              <div className={styles['small-card-footer']}>
                <div className="row justify-content-between">
                  <div className="col-auto">
                    <p className={styles.address}>{this.conseil.destination_hash}</p>
                  </div>
                  <div className="col-auto" style={{ padding: "0" }}>
                    <CopyInfo id="copyData" text={this.conseil.destination_hash} />
                  </div>
                </div>
              </div>
            </div>
            <p className={styles.explain}>After you transfer the amount and the transaction is confirmed by the Tezos network, you'll be automatically redirected to the success page.
              </p>
          </div>
        </div>
      </div>
    )
  }
}
