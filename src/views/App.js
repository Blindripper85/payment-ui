/* eslint-disable no-unused-vars */
import React from 'react';
import QR from './QR';
import Success from './Success';
import Expired from './Expired';
import Loading from './Loading';
import '../../node_modules/react-circular-progressbar/dist/styles.css';
import '../../node_modules/video-react/dist/video-react.css';
import 'Root/styles/base.less';

export default class extends React.Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      qr: false,
      success: false,
      expired: false,
    }

    const urlParams = new URLSearchParams(window.location.search);
    const PAYZOS_PAYMENT_ID = urlParams.get('payment_id');
    if (typeof PAYZOS_PAYMENT_ID === undefined || typeof PAYZOS_API_URL === undefined) {
      this.setState({
        loading: false,
        qr: false,
        success: false,
        expired: true,
      })
      return false;
    }
    this.payment_data(PAYZOS_API_URL, PAYZOS_PAYMENT_ID);
  }

  async payment_data(endpoint, payment_id) {
    const url = endpoint + "payment_info?payment_id=" + payment_id;
    const response = await fetch(url, {
      method: "GET",
      headers: {
        'Content-Type': 'application/json'
      },
    });
    const json = await response.json();
    if (typeof json.ok === undefined || !json.ok) {
      return false;
    }
    console.log(json.data);

    this.setState({
      payment_data: json.data,
      loading: false,
      qr: (json.data.status === "await") ? true : false,
      success: (json.data.status === "done") ? true : false,
      expired: (json.data.status === "fail") ? true : false,
    });
    return true;
  }

  render() {
    return (
      <div>
        {this.state.loading && <Loading />}
        {this.state.qr && <QR
          amount={this.state.payment_data.amount}
          destination_hash={this.state.payment_data.destination_hash}
          payment_id={this.state.payment_data.id}
          start_time={this.state.payment_data.start_time * 1000}
        />}
        {this.state.success && <Success
          transaction_hash={this.state.payment_data.transaction_hash}
          finish_time={this.state.payment_data.finish_time}
          amount={this.state.payment_data.amount / 1000000}
          amount_orginal={this.state.payment_data.amount_orginal}
          currency_orginal={this.state.payment_data.currency_orginal}
        />}
        {this.state.expired && <Expired />}
      </div>
    );
  }
}


