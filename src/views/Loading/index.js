import React from 'react';
import './style.css';

const Loading = (props) => (
    <>
        <div className="row justify-content-center align-items-center h-100">
            <div className=" my-4" style={{ paddingBottom: "150px" }}>
                <div className="spinner">
                    <div className="bounce1"></div>
                    <div className="bounce2"></div>
                    <div className="bounce3"></div>
                </div>
            </div>
        </div>
    </>
);

Loading.propTypes = {};

export default Loading;
