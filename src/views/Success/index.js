import React from 'react';
import medal from 'Root/assets/images/medal.png';
import styles from './styles.less';


export default class Success extends React.Component {

  constructor(props) {
    super(props);
    this.props = props;
    // this.state = {
    //   finish_time: this.props.finish_time
    // }
    const monthNames = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];
    const date = new Date(this.props.finish_time * 1000)
    this.state = {
      finish_time: date.getDate() + " " + monthNames[date.getMonth()] + " " + date.getFullYear()
    };
    this.data = [
      { subject: 'Transaction hash', value: this.props.transaction_hash },
      { subject: 'Amount', value: `${this.props.amount_orginal} ${this.props.currency_orginal}`, currency: `${this.props.amount} XTZ` },
      { subject: 'Date', value: this.state.finish_time },
    ];

  }

  render() {
    let i = 0;
    return (
      <div className="row justify-content-center align-items-center h-100">
        <div className="card my-4" style={{ width: "500px" }}>
          <div className={styles['card-body']}>
            <img
              src={medal}
              alt="medal"
              className="d-block mx-auto"
              width="61px"
              height="71px"
            />
            <h1 className={styles.title}>Payment successful</h1>
            <ul className={styles.list}>
              {this.data.map((item, index) => (

                <li key={++i}>
                  <div className="row justify-content-between">
                    <div className="col-auto">
                      <h3 className={styles.subject}>{item.subject}</h3>
                    </div>
                    <div className="col-auto">
                      {(index === 0)
                        && (
                          <a target="_blank" href={`https://tezblock.io/transaction/${item.value}`} className={styles.explain}>{item.value.slice(0, 15) + "..."}</a>
                        )}
                      {(index === 1)
                        && (
                          <p className={styles.explain}>
                            {item.value} <span className={styles.seprator}>|</span>
                            <span className={styles.currency}> {item.currency}</span>
                          </p>
                        )}
                      {(index == 2)
                        && <p className={styles.explain}>{item.value}</p>}
                    </div>
                  </div>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    );
  }

}

