export const homePage = '/';
export const QRPage = '/QR';
export const successPage = '/success';
export const expiredPage = '/expired';
export const guidePage = '/guide';
export const FAQPage = '/faq';
