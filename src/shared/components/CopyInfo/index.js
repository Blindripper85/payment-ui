import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Clipboard from 'react-clipboard.js';
import { Popover } from 'reactstrap';
import styles from './styles.less';

const CopyInfo = (props) => {
  const [popoverOpen, handlePopover] = useState(false);
  const toggle = () => {
    handlePopover(true);
    setTimeout(() => {
      handlePopover(false);
    }, 1200);
  };

  return (
    <>
      <div className={styles.clipboard}>
        <div id={props.id}>
          <Clipboard
            className="btn p-0"
            style={{ borderRadius: '0' }}
            data-clipboard-text={props.text}
          >
            {popoverOpen
              ? <span className="icon icon-check-circle f-15" />
              : <span className="icon icon-copy f-14" />}
          </Clipboard>
        </div>
        <Popover
          placement="top"
          isOpen={popoverOpen}
          target={props.id}
          toggle={toggle}
          className={styles.popover}
        >Copied!
        </Popover>
      </div>
    </>
  );
};

CopyInfo.propTypes = {
  id: PropTypes.string.isRequired,
  text: PropTypes.any.isRequired,
};

export default CopyInfo;
