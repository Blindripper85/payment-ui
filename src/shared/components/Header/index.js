import React, { useState } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link, NavLink } from 'react-router-dom';
import logo from 'Root/assets/images/logo.png';
import { homePage, guidePage, FAQPage } from 'Root/constants/routes';
import prestashop from 'Root/assets/images/prestashop.png';
import magento from 'Root/assets/images/magento.png';
import woocommerce from 'Root/assets/images/woocommerce.png';
import openCart from 'Root/assets/images/webshop.png';
import styles from './styles.less';

const dropDowns = [
  {
    text: 'Woo commerce', image: woocommerce, width: '16px', height: '10px', link: '/', disabled: false,
  },
  {
    text: 'Prestashop', image: prestashop, width: '16px', height: '16px', link: '/', disabled: true,
  },
  {
    text: 'Magento', image: magento, width: '14px', height: '16px', link: '/', disabled: true,
  },
  {
    text: 'Open cart', image: openCart, width: '16px', height: '11px', link: '/', disabled: true,
  },
];

const Header = (props) => {
  const [active, setActive] = useState(false);

  const toggleMenu = () => {
    setActive(!active);
  };

  return (
    <>
      <nav className={classNames('navbar navbar-expand-lg', styles.nav)}>
        <Link to={homePage} className="navbar-brand">
          <img src={logo} width="30px" height="40px" alt="payzos" />
        </Link>
        {/* responsive menu */}
        <div className={classNames('d-xl-none d-lg-none d-md-flex d-sm-flex d-flex', styles['burger-menu'])}>
          <div
            className={classNames('button_container', active ? 'active' : '')}
            id="toggle"
            onClick={toggleMenu}
          >
            <span className="top" />
            <span className="middle" />
            <span className="bottom" />
          </div>
          <div className={classNames('overlay', active ? 'open' : '')} id="overlay">
            <nav className="overlay-menu">
              <ul>
                <li><Link to={homePage}>Home</Link></li>
                <li><Link to="/">Woo commerce</Link></li>
                <li><a className="not-allowed-cursor">Prestashop</a></li>
                <li><a className="not-allowed-cursor">Magento</a></li>
                <li><a className="not-allowed-cursor">Opencart</a></li>
                <li><Link to={guidePage}>Guide</Link></li>
                <li><Link to={FAQPage}>FAQ</Link></li>
              </ul>
            </nav>
          </div>
        </div>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item">
              <NavLink to={homePage} className="nav-link" exact activeClassName="is-active">Home</NavLink>
            </li>
            <li className="nav-item">
              <div className="popover-wrapper">
                <button type="button" className="btn popover-title">
                  Plugins <span className="icon-angle-down pl-1 f-9" />
                </button>
                <div className="popover-content">
                  <div className="popover-arrow" />
                  <ul className={styles.list}>
                    {dropDowns.map((item, index) => (
                      <li key={index}>
                        {item.disabled ? (
                          <a style={{ cursor: 'not-allowed' }}>
                            <img src={item.image} width={item.width} height={item.height} alt="logo" />
                            {item.text} <span className={styles.soon}>soon</span>
                          </a>
                        ) : (
                          <a href={item.link} style={{ color: '#2f2f2f' }}>
                            <img src={item.image} width={item.width} height={item.height} alt="logo" />
                            {item.text}
                          </a>
                        )}
                      </li>
                    ))}
                  </ul>
                </div>
              </div>
            </li>
            <li className="nav-item">
              <NavLink to={guidePage} className="nav-link" exact activeClassName="is-active">Guide</NavLink>
            </li>
            <li className="nav-item">
              <NavLink to={FAQPage} className="nav-link" exact activeClassName="is-active">FAQ</NavLink>
            </li>
          </ul>
        </div>
      </nav>
    </>
  );
};

Header.propTypes = {

};

export default Header;
