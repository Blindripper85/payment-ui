import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './styles.less';

const Footer = (props) => (
  <>

    <div className={styles.footer}>
      <div className="content-box">
        <div className="row justify-content-between">
          <div className="col-auto">
            <p className={styles['copy-right']}>Copyright 2019</p>
          </div>
          <div className="col-auto pt-1">
            <a href="/" className={classNames(styles.icon)}>
              <span className="icon-twitter" />
            </a>
          </div>
        </div>
      </div>
    </div>
  </>
);

Footer.propTypes = {

};

export default Footer;
