import React from 'react';
import Countdown from 'react-countdown-now';
import { buildStyles, CircularProgressbar } from 'react-circular-progressbar';
import classNames from 'classnames';
import ChangingProgressProvider from 'Root/shared/components/ChangingProgressProvider';
import PropTypes from 'prop-types';
import styles from './styles.less';

const renderer = ({ minutes, seconds }) => (<span className={styles.timer}>{minutes}:{seconds}</span>);

const CountDownDuration = ({ duration, passedTime }) => {
  const time = [];
  const timePlus = [];
  for (let i = 0; i < duration; i++) {
    time.push((1 / duration) * 100);
  }
  if (time.length > 0) {
    time.reduce((previousValue, currentValue) => {
      timePlus.push(Math.round((previousValue + currentValue) * 100) / 100);
      return previousValue + currentValue;
    });
  }
  return (
    <>
      <div className={classNames(styles.box, 'center-content-col')}>
        <span className={styles.count}>
          <Countdown
            date={Date.now() + ((duration) * 1000)}
            renderer={renderer}
          />
        </span>
        <span className={classNames(styles.circle, 'float-right')}>
          <ChangingProgressProvider values={timePlus} passedTime={passedTime}>
            {(percentage) => (
              <CircularProgressbar
                value={percentage}
                strokeWidth={8}
                styles={buildStyles({
                  pathTransitionDuration: 1,
                  textSize: '16px',
                })}
              />
            )}
          </ChangingProgressProvider>
        </span>
      </div>
    </>
  );
};

CountDownDuration.propTypes = {

};

export default CountDownDuration;
